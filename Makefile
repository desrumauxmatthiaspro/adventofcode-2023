build:
	mkdir -p build-aoc
	cd build-aoc && cmake .. && make

day_%:
	$(shell find build-aoc -type f -name $@)