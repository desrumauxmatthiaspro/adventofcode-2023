//
// Created by desrumaux on 07/12/23.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <deque>

#include <peglib.h>

struct card {
    long id;
    std::vector<long> winning_numbers;
    std::set<long> numbers;
};

int main(int argc, char** argv) {
    std::ifstream ifs;

    {
        std::ostringstream oss;
        oss << CMAKE_SOURCE_DIR << "/day_4/ex_2/input";
        ifs.open(oss.str());
    }

    peg::parser parser{R"(
        INPUT <- 'Card' NUMBER ':' WINING_NUMBERS '|' NUMBERS
        WINING_NUMBERS <- NUMBER+
        NUMBERS <- NUMBER+
        NUMBER <- < [0-9]+ >
        %whitespace <- [ \t]*
    )"};
    parser.enable_packrat_parsing();

    parser["NUMBER"] = [](const peg::SemanticValues& sv) {
        return std::stol(sv.token_to_string());
    };

    parser["NUMBERS"] = [](const peg::SemanticValues& sv) {
        std::set<long> numbers;
        for (auto& item : sv) {
            numbers.insert(std::any_cast<long>(item));
        }
        return numbers;
    };

    parser["WINING_NUMBERS"] = [](const peg::SemanticValues& sv) {
        std::vector<long> numbers;
        for (auto& item : sv) {
            numbers.emplace_back(std::any_cast<long>(item));
        }
        return numbers;
    };

    parser["INPUT"] = [](const peg::SemanticValues& sv) {
        card card;
        card.id = std::any_cast<long>(sv[0]);
        card.winning_numbers = std::any_cast<std::vector<long>>(sv[1]);
        card.numbers = std::any_cast<std::set<long>>(sv[2]);
        return card;
    };

    std::deque<long> scratchcard_counter_queue;
    long total = 0;
    int i = -1;
    std::string line;
    while (!ifs.eof()) {
        i++;
        std::getline(ifs, line);

        if (line.empty()) {
            continue;
        }

        card card;
        if (!parser.parse(line, card)) {
            std::cout << "Unable to parse " << i << " line." << std::endl
                      << line << std::endl;
            return 1;
        }

        long count = 1;
        if (!scratchcard_counter_queue.empty()) {
            count += scratchcard_counter_queue.front();
            scratchcard_counter_queue.pop_front();
        }

        long value = 0;
        for (auto& number : card.winning_numbers) {
            if (card.numbers.find(number) == card.numbers.end()) {
                continue;
            }
            value++;
        }

        size_t j = 0;
        for (; j < value && j < scratchcard_counter_queue.size(); j++) {
            scratchcard_counter_queue[j] += count;
        }

        for (; j < value; j++) {
            scratchcard_counter_queue.push_back(count);
        }

        total += count;
    }

    std::cout << "Read " << i << " lines." << std::endl
              << "The result is " << total << std::endl;
    return 0;
}
