# Advent of code 2023

Every puzzle will be solved with the C++ language.

## Test my code

You will require these packages:

- cmake
- build-essential

```bash
make build
make day_{DAY NUMBER}_ex_{EXERCISE NUMBER}
```

## My progress:

- [x] Day 1
  - [x] Exercise 1
  - [x] Exercise 2
- [x] Day 2
  - [x] Exercise 1
  - [x] Exercise 2
- [x] Day 3
  - [x] Exercise 1
  - [x] Exercise 2
- [x] Day 4
  - [x] Exercise 1
  - [x] Exercise 2

## 3rd-party

- [peglib.h](https://github.com/yhirose/cpp-peglib)