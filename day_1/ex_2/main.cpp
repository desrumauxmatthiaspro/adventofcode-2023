//
// Created by desrumaux on 05/12/23.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <regex>
#include <vector>

#include "macro.hpp"

int main(int argc, char** argv) {
    std::map<std::string, long> numbersAsText{
        {"zero", 0},
        {"one", 1},
        {"two", 2},
        {"three", 3},
        {"four", 4},
        {"five", 5},
        {"six", 6},
        {"seven", 7},
        {"eight", 8},
        {"nine", 9},
    };

    std::regex frontRegex;
    {
        std::ostringstream oss;
        oss << "(";
        bool first = true;
        for (const auto& item : numbersAsText) {
            if (!first) {
                oss << "|";
            }
            oss << item.first;
            first = false;
        }
        oss << ")";
        frontRegex = std::regex{oss.str()};
    }

    std::regex backRegex;
    {
        std::ostringstream oss;
        oss << "(";
        bool first = true;
        for (const auto& item : numbersAsText) {
            auto copy = item.first;
            std::reverse(copy.begin(), copy.end());

            if (!first) {
                oss << "|";
            }
            oss << copy;
            first = false;
        }
        oss << ")";
        backRegex = std::regex{oss.str()};
    }

    std::ifstream ifs;

    {
        std::ostringstream oss;
        oss << CMAKE_SOURCE_DIR << "/day_1/ex_2/input";
        ifs.open(oss.str());
    }

    long total = 0;
    int i = -1;
    while (!ifs.eof()) {
        std::string line;

        i++;
        std::getline(ifs, line);
        if (line.empty()) {
            continue;
        }

        // lookup for numbers
        size_t fst = -1;
        size_t snd = -1;
        std::string numbers;
        int j = -1;
        for (const auto& c : line) {
            j++;
            if (c >= '0' && c <= '9') {
                if (fst == -1) {
                    fst = j;
                }
                snd = j;
                numbers += c;
            }
        }

        std::string value;
        {
            std::ostringstream oss;
            oss << numbers.front() << numbers.back();
            value = oss.str();
        }

        // lookup for number as text

        /**
         * First case we have a number as text before the first digit.
         * It's pretty simple one we got the match.
         * If the match is before the index of first digit then we replace the first digit of this value.
         */
        std::smatch match;
        if (std::regex_search(line, match, frontRegex)) {
            if (match.position() < fst) {
                value[0] = '0' + numbersAsText[match.str()];
            }
        }

        /**
         * Second case we have a number as text after the last digit.
         * It can be hard due to the fact you can got this case: ...5oneight
         * If we only replace text with digit we will got the wrong number because eight is the last true number.
         * So we reverse the string and regex to get the real last digit.
         */
        std::reverse(line.begin(), line.end());
        if (std::regex_search(line, match, backRegex)) {
            auto number = match.str();
            std::reverse(number.begin(), number.end());
            auto position = line.length() - match.position() - number.length();
            if (position > snd) {
                value[1] = '0' + numbersAsText[number];
            }
        }
        std::reverse(line.begin(), line.end());

        std:: cout << i << ": " << value << std::endl;
        total += std::stol(value);
    }

    std::cout << "Read " << i << " lines." << std::endl
        << "The result is " << total << std::endl;
    return 0;
}
