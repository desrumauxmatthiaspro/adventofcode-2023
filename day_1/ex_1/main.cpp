//
// Created by desrumaux on 05/12/23.
//

#include <iostream>
#include <sstream>
#include <fstream>

#include "macro.hpp"

int main(int argc, char** argv) {
    std::ifstream ifs;

    {
        std::ostringstream oss;
        oss << CMAKE_SOURCE_DIR << "/day_1/ex_1/input";
        ifs.open(oss.str());
    }

    long total = 0;
    int i = -1;
    std::string line;
    while (!ifs.eof()) {
        i++;
        std::getline(ifs, line);
        if (line.empty()) {
            continue;
        }

        std::string numbers;
        for (const auto& c : line) {
            if (c >= '0' && c <= '9') {
                numbers += c;
            }
        }

        std::string value;
        {
            std::ostringstream oss;
            oss << numbers.front() << numbers.back();
            value = oss.str();
        }

        std:: cout << i << ": " << value << std::endl;
        total += std::stol(value);
    }

    std::cout << "Read " << i << " lines." << std::endl
        << "The result is " << total << std::endl;
    return 0;
}
