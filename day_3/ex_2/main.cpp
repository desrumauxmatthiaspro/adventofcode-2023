//
// Created by desrumaux on 07/12/23.
//
//
// Created by desrumaux on 07/12/23.
//

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <set>
#include <memory>

#include "macro.hpp"

struct grid_item {
    std::shared_ptr<long> number;
    bool symbol;
};

bool exist(const std::vector<std::vector<grid_item>>& grid, size_t x, size_t y) {
    if (x < 0 || y < 0) {
        return false;
    } else if (grid.size() <= y) {
        return false;
    } else if (grid.at(y).size() <= x) {
        return false;
    } else {
        return true;
    }
}

void add(const grid_item& item, std::set<std::shared_ptr<long>>& numbers) {
    if (nullptr == item.number || numbers.find(item.number) != numbers.end()) {
        return;
    }

    numbers.insert(item.number);
}

int main(int argc, char** argv) {
    std::ifstream ifs;

    {
        std::ostringstream oss;
        oss << CMAKE_SOURCE_DIR << "/day_3/ex_2/input";
        ifs.open(oss.str());
    }

    std::vector<std::vector<grid_item>> grid;

    int i = -1;
    std::string line;
    while (!ifs.eof()) {
        i++;
        std::getline(ifs, line);

        std::shared_ptr<long> ptr;
        std::string acc;
        auto& row = grid.emplace_back();
        for (const auto& c : line) {
            if (c >= '0' && c <= '9') {
                if (acc.empty()) {
                    ptr = std::make_shared<long>(0);
                }
                acc += c;
                row.emplace_back(grid_item{ptr, false});
            } else {
                if (!acc.empty()) {
                    *ptr = std::stol(acc);
                    acc = "";
                    ptr = nullptr;
                }

                row.emplace_back(grid_item{nullptr, c != '.'});
            }
        }

        if (!acc.empty()) {
            *ptr = std::stol(acc);
            acc = "";
            ptr = nullptr;
        }
    }

    long total = 0;
    for (size_t y = 0; y < grid.size(); y++) {
        auto& row = grid.at(y);
        for (size_t x = 0; x < row.size(); x++) {
            auto& item = row.at(x);
            if (!item.symbol) {
                continue;
            }

            std::set<std::shared_ptr<long>> numbers;
            if (exist(grid, x - 1, y - 1)) {
                add(grid[y-1][x-1], numbers);
            }

            if (exist(grid, x, y - 1)) {
                add(grid[y-1][x], numbers);
            }

            if (exist(grid, x + 1, y - 1)) {
                add(grid[y-1][x+1], numbers);
            }

            if (exist(grid, x - 1, y)) {
                add(grid[y][x-1], numbers);
            }

            if (exist(grid, x + 1, y)) {
                add(grid[y][x+1], numbers);
            }

            if (exist(grid, x - 1, y + 1)) {
                add(grid[y+1][x-1], numbers);
            }

            if (exist(grid, x, y + 1)) {
                add(grid[y+1][x], numbers);
            }

            if (exist(grid, x + 1, y + 1)) {
                add(grid[y+1][x+1], numbers);
            }

            if (numbers.size() == 2) {
                long ratio = 1;
                for (auto& it : numbers) {
                    ratio *= *it;
                }
                total += ratio;
            }
        }
    }

    std::cout << "Read " << i << " lines." << std::endl
              << "The result is " << total << std::endl;
    return 0;
}
