//
// Created by desrumaux on 05/12/23.
//

#include <iostream>
#include <fstream>
#include <vector>

#include <peglib.h>

struct game_info {
    long id;
};

struct game_set {
    long green;
    long blue;
    long red;
};

struct game {
    game_info info;
    std::vector<game_set> sets;
};

int main(int argc, char** argv) {
    std::ifstream ifs;

    {
        std::ostringstream oss;
        oss << CMAKE_SOURCE_DIR << "/day_2/ex_2/input";
        ifs.open(oss.str());
    }

    peg::parser parser{R"(
        INPUT <- GAME_INFO (GAME_SET ';' / GAME_SET)+
        GAME_INFO <- 'Game' NUMBER ':'
        GAME_SET <- (NUMBER COLOR ',' / NUMBER COLOR)+
        COLOR <- 'blue' / 'red' / 'green'
        NUMBER <- < [0-9]+ >
        %whitespace <- [ \t]*
    )"};
    parser.enable_packrat_parsing();

    parser["NUMBER"] = [](const peg::SemanticValues& sv) {
        return std::stol(sv.token_to_string());
    };

    parser["COLOR"] = [](const peg::SemanticValues& sv) {
        return sv.token_to_string();
    };

    parser["GAME_SET"] = [](const peg::SemanticValues& sv) {
        game_set set{0,0,0};
        for (size_t i = 0; i < sv.size(); i+=2) {
            const auto value = std::any_cast<long>(sv[i]);
            const auto color = std::any_cast<std::string>(sv[i+1]);
            if (color == "red") {
                set.red = value;
            } else if (color == "blue") {
                set.blue = value;
            } else if (color == "green") {
                set.green = value;
            }
        }
        return set;
    };

    parser["GAME_INFO"] = [](const peg::SemanticValues& sv) {
        return game_info{
            std::any_cast<long>(sv[0]),
        };
    };

    parser["INPUT"] = [](const peg::SemanticValues& sv) {
        game game;
        game.info = std::any_cast<game_info>(sv[0]);
        for (size_t i = 1; i < sv.size(); i++) {
            game.sets.push_back(std::any_cast<game_set>(sv[i]));
        }
        return game;
    };

    long total = 0;
    int i = -1;
    std::string line;
    while (!ifs.eof()) {
        i++;
        std::getline(ifs, line);

        if (line.empty()) {
            continue;
        }

        game game;
        if (!parser.parse(line, game)) {
            std::cout << "Unable to parse " << i << " line." << std::endl
                << line << std::endl;
            return 1;
        }

        long max_red = 0;
        long max_green = 0;
        long max_blue = 0;

        for (auto& set : game.sets) {
            if (max_red < set.red) {
                max_red = set.red;
            }

            if (max_green < set.green) {
                max_green = set.green;
            }

            if (max_blue < set.blue) {
                max_blue = set.blue;
            }
        }

        total += max_red * max_green * max_blue;
    }

    std::cout << "Read " << i << " lines." << std::endl
              << "The result is " << total << std::endl;
    return 0;
}
